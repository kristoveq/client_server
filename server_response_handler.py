import json
from datetime import datetime as dt


class ResponseHandler:
    def __init__(self, server, user):
        self.server = server
        self.user = user

    def unrecognized_command_re(self, request=None):
        return json.dumps('Unrecognized command.')

    def uptime_re(self, request=None):
        now = dt.now().replace(microsecond=0)
        result = now - self.server.server_start
        return json.dumps(str(result))

    def info_re(self, request=None):
        return json.dumps(self.server.server_info)

    def help_re(self, request=None):
        commands_list = '\n'.join([f"{command} - {description}" for command, description in self.user.commands.items()])
        return json.dumps(commands_list)

    def stop_re(self, request=None):
        return json.dumps('App termination.')

    def login_re(self, request=None):
        data = request.split(',')
        name, password = data[1], data[2]

        if self.user.log_in(name, password):
            return json.dumps('LOGGED IN')
        else:
            return json.dumps('\nIncorrect login details')

    def sign_up_re(self, request=None):
        data = request.split(',')
        name, password = data[1], data[2]

        if self.user.sign_up(name, password):
            return json.dumps(("\nSIGNED UP!\n"
                               "Now please log in with your nickname and password."))
        else:
            return json.dumps('Username already exist.')

    def send_re(self, request=None):
        data = list(request.split(',')[1:])
        typed_user = data[0]
        message = ','.join(data[1:])
        if not self.user.check_picked_user_exist(typed_user):
            return json.dumps('Typed user doesn\'t exist.')
        elif self.user.check_msg_box_overflow(typed_user):
            return json.dumps('Recipient\'s inbox is full.')

        if not self.user.send_message(typed_user, message):
            return json.dumps('Message is too long (limit = 255 characters).')
        else:
            return json.dumps('Message successfully sent')

    def check_re(self, request=None):
        if self.user.check_messages():
            return json.dumps(self.user.show_messages())
        else:
            return json.dumps('Message box is empty')

    def delete_re(self, request=None):
        user_input = request.split(',')[1]
        if not self.user.check_messages():
            return json.dumps('Message box is empty')
        elif not self.user.check_int_input(user_input):
            return json.dumps('Enter only numeric position')

        if self.user.delete_message(user_input):
            return json.dumps('Message deleted')
        else:
            return json.dumps('Choice out of reach')

    def add_re(self, request=None):
        name = request.split(',')[1]
        pswrd = request.split(',')[2]
        self.user.add_user_data(name, pswrd)
        return json.dumps(f'User: "{name}" added to users database.')

    def change_password_re(self, request=None):
        name = request.split(',')[1]
        new_pswrd = request.split(',')[2]
        if not self.user.check_picked_user_exist(name):
            return json.dumps('Typed user doesn\'t exist.')
        self.user.change_password(name, new_pswrd)
        return json.dumps(f'{name} password was changed successfully.')

    def del_user_re(self, request=None):
        name = request.split(',')[1]
        if name == self.user.name:
            return json.dumps('Cannot remove yourself.')
        elif not self.user.check_picked_user_exist(name):
            return json.dumps('Unknown user')
        else:
            self.user.delete_user(name)
            return json.dumps(f'User: "{name}" was successfully deleted from database.')

    def set_role_re(self, request=None):
        name = request.split(',')[1]
        new_role = request.split(',')[2]
        if name == self.user.name:
            return json.dumps('Can\'t change the role for yourself.')
        elif not self.user.check_picked_user_exist(name):
            return json.dumps('Unknown user.')

        if self.user.change_role(name, new_role):
            return json.dumps(f'{name} role successfully changed to {new_role}.')
        else:
            return json.dumps('Incorrect role.')

    def out_re(self, request=None):
        self.user.log_out()
        return json.dumps('Logged out successfully')
