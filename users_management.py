import commands
# from postgre.users_db import users_db, msg_db, db_handler
from sqlite3_users_db import users_db, msg_db, db_handler


class User:
    def __init__(self, name, pswrd, role):
        self.id = None
        self.name = name
        self.password = pswrd
        self.role = role
        self.commands = commands.basic_commands
        self.msg_box = []
        self.logged_in = False
        self.msg_limit = 0

    def grant_permissions(self):
        if self.role == 'user':
            self.commands = {**commands.basic_commands, **commands.user_commands}
            self.msg_limit = 5
        elif self.role == 'administrator':
            self.commands = {**commands.basic_commands, **commands.user_commands, **commands.admin_commands}
            self.msg_limit = 100
        else:
            self.commands = commands.basic_commands
            self.msg_limit = 0

        if self.logged_in:
            del self.commands['login']
            del self.commands['signup']

    def add_user_data(self, name, pswrd):
        users_db.add_new_user_in_db(name, pswrd)

    def log_in(self, name, pswrd):
        users_database = users_db.get_users_data_from_db()
        factor = next((x for x in users_database if x[1] == name and x[2] == pswrd), False)
        if factor is not False:
            self.id = factor[0]
            self.name = factor[1]
            self.password = factor[2]
            self.role = factor[3]
            self.logged_in = True
            self.grant_permissions()
            return True
        return False

    def sign_up(self, name, pswrd):
        if name in users_db.get_users_names_from_db():
            return False
        else:
            users_db.add_new_user_in_db(name, pswrd)
            return True

    def log_out(self):
        self.name = None
        self.password = None
        self.role = None
        self.msg_box = []
        self.msg_limit = 0
        self.logged_in = False
        self.grant_permissions()

    def check_picked_user_exist(self, typed_user):
        if typed_user in users_db.get_users_names_from_db():
            return True
        else:
            return False

    def evaluate_message_data(self, message):
        if len(message) > 255:
            return False
        else:
            result = [self.name, message]
            return result

    def check_msg_box_overflow(self, name):
        msg_limit = users_db.get_single_user_data_from_db(name)[4]
        msg_box_capabilities = len(msg_db.get_user_messages_from_db(name))
        if msg_box_capabilities < msg_limit:
            return False
        else:
            return True

    def send_message(self, typed_user, message):
        checked_msg = self.evaluate_message_data(message)
        if type(checked_msg) is list:
            msg_db.insert_new_msg_to_user_msg_box(typed_user, self.name, message)
            return True
        else:
            return False

    def check_messages(self):
        if len(msg_db.get_user_messages_from_db(self.name)) == 0:
            return False
        else:
            return True

    def show_messages(self):
        result = []
        messages = msg_db.get_user_messages_from_db(self.name)
        for pos, msg in enumerate(messages, start=1):
            corrected_msg = f'{pos}.\n{msg}'
            result.append(corrected_msg)
        return '\n'.join(result)

    def check_int_input(self, data):
        if data.isdigit():
            return True
        else:
            return False

    def delete_message(self, msg_id):
        if not int(msg_id) in range(1, len(msg_db.get_user_messages_from_db(self.name)) + 1):
            return False
        else:
            msg_db.delete_user_msg_from_db_by_id(self.name, msg_id)
            return True

    def change_password(self, name, new_password):
        users_db.change_user_value_in_db(name, 'password', new_password)

    def delete_user(self, name):
        users_db.delete_data_from_db(name)

    def change_role(self, name, new_role):
        roles = ['user', 'administrator']
        if not new_role in roles:
            return False
        else:
            if new_role == 'administrator':
                limit = '100'
            else:
                limit = '5'
        users_db.change_user_value_in_db(name, 'role', new_role)
        users_db.change_user_value_in_db(name, 'msg_limit', limit)
        return True

    def stop_connection(self):
        db_handler.close_connection()


user = User(None, None, None)
