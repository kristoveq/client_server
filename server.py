import socket
import sys
from datetime import datetime as dt
import json
from users_management import user
from server_response_handler import ResponseHandler


class Server:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.BUFFER = 1024
        self.server_info = 'Version: 1.4 | Creation date: 31.01.2023'
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self):
        try:
            self.server_socket.bind((self.host, self.port))
            self.server_start = dt.now().replace(microsecond=0)
            self.server_socket.listen(3)
            self.client_socket, self.address = self.server_socket.accept()
            return True
        except socket.error as e:
            print(f'Socket error occurred: {e}')
            self.server_socket.close()
            return False
        except Exception as e:
            print(f'An unexpected error occurred: {e}')
            self.server_socket.close()
            return False

    def take_request(self):
        try:
            request = self.client_socket.recv(self.BUFFER).decode("utf-8")
            return request
        except socket.error as e:
            print(f'Socket error occurred: {e}')
            self.server_socket.close()
            return False
        except ConnectionResetError as e:
            print(f'Connection interrupted by client: {e}')
            self.server_socket.close()
            return False
        except Exception as e:
            print(f'An unexpected error occurred {e}')
            self.server_socket.close()
            return False

    def prepare_response(self, request):
        response_handler = ResponseHandler(self, user)
        response_methods = {
            'unrecognized_command': response_handler.unrecognized_command_re,
            'uptime': response_handler.uptime_re,
            'info': response_handler.info_re,
            'help': response_handler.help_re,
            'stop': response_handler.stop_re,
            'login': response_handler.login_re,
            'signup': response_handler.sign_up_re,
            'send': response_handler.send_re,
            'check': response_handler.check_re,
            'delete': response_handler.delete_re,
            'add': response_handler.add_re,
            'change_password': response_handler.change_password_re,
            'del_user': response_handler.del_user_re,
            'set_role': response_handler.set_role_re,
            'out': response_handler.out_re
        }
        command = request.split(',')[0]
        if command in response_methods:
            response_method = response_methods[command]
            return response_method(request)
        else:
            return response_handler.unrecognized_command_re()

    def send_commands(self):
        try:
            update_commands = json.dumps([key for key in user.commands.keys()])
            self.send_response(update_commands)
        except TypeError as e:
            print(f'Error during serializng command to JSON: {e}')
            self.send_response(json.dumps({'Error': 'Failed to serialize commands.'}))
            return False
        except (ConnectionResetError, socket.error) as e:
            print(f'Error during sending response: {e}')
            self.server_socket.close()
            return False

    def send_response(self, response):
        try:
            self.client_socket.send(response.encode('utf-8'))
        except ConnectionResetError:
            self.server_socket.close()


if __name__ == '__main__':

    HOST = '127.0.0.1'
    PORT = 61234

    server = Server(HOST, PORT)
    server.connect()

    while True:

        server.send_commands()
        request = server.take_request()
        response = server.prepare_response(request)
        server.send_response(response)

        if request == "stop":
            user.stop_connection()
            server.server_socket.close()
            sys.exit()
