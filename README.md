# Client_Server

Simple C/S application based on socket connection, can be used for communication between users. Client side of app allows user to create new account, log in to existing account, manage inbox and sending messages to other users.
On the other hand admin has more options to use (including creating account for new user, changing passwords, deleting user accounts, changing role of regular user to administrator and couple more options concerning app operation).  
Whole app database is based on PostgreSQL.  


### Versions:

1.0 - A simple application based on a client-server connection and information exchange based on the JSON format.  
1.1 - The application has been enhanced to include the ability to create users and manage processes related to them (JSON as database).  
1.2 - Unit tests have been added to the application.  
1.3 - The database format has been changed from JSON to PostgreSQL.  

## Run application

To ensure that the application starts correctly, make sure you have installed Python 3.11 or above and follow these steps:  

- Clone repository to your local machine,  
- Install necessary modules:  
```

pip install -r requirements.txt

```  
- In the first step of run, pick scrypt server.py,  
- Then - in another terminal - run client.py file,  
- Follow displayed instructions to be able to use the application possibilities

## Example use

- As a first step, after starting the application, follow the guidelines:  
```python

Type "help" for support:
help
help - show list of possible actions with short description
signup - create a new account
login - log into your personal account

```

### Logging

- Use 'login' command and enter username and password. When it is done correctly server returns relevant message.  

```python

Type "help" for support:
login
Enter your nickname: user
Enter password: 4321
LOGGED IN

```

### Create new account

- Use 'signup' command and enter username and password. If username is not in database, server returns relevant message.

```python

signup
Enter your nickname: new_user
Enter password: some_password

SIGNED UP!
Now please log in with your nickname and password.

```

## Available commands

- Basic commands:  

'help' : show list of possible actions with short description.  
'signup' : create a new account.  
'login' : log into your personal account.

- Commands available for user:

'send' : send message to specific user.  
'check' : check message box.  
'delete' : delete specific message.  
'out' : log off.

- Commands available only for administrator ( + user commands):

'uptime': show server lifetime.  
'info' : show server version and date of creation.  
'help' : show list of possible actions with short description.  
'stop' : stop connection.  
'add' : adding user.  
'change_password' : change password of specific account.  
'del_user' : delete user.  
'set_role' : set user/admin role.  

- IMPORTANT   

If in doubt, the 'help' command is always available to display the current options.