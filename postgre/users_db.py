import psycopg2
from psycopg2 import extras
from psycopg2 import Error
from postgre.config_db import config


class DatabaseHandler:
    def __init__(self):
        self.params = None
        self.conn = None
        self.standard_cur = None
        self.dict_cur = None

    def connect(self, configuration):
        try:
            self.params = configuration
            self.conn = psycopg2.connect(**self.params)
            self.conn.autocommit = False
            self.standard_cur = self.conn.cursor()
            self.dict_cur = self.conn.cursor(cursor_factory=extras.DictCursor)

        except (Exception, Error) as error:
            print(f'Connection to database FAILED: \n{error}')

    def close_connection(self):
        if self.conn is not None:
            try:
                self.standard_cur.close()
                self.dict_cur.close()
                self.conn.close()
                print('Database connection closed.')
            except (Exception, Error) as error:
                print(f'Can\'t close connection with database: \n{error}')


class UserHandler:
    def __init__ (self, database_handler):
        self.db_handler = database_handler
        self.standard_cur = self.db_handler.standard_cur
        self.dict_cur = self.db_handler.dict_cur
        self.conn = self.db_handler.conn

    def table_exist(self):
        try:
            check_query = 'SELECT EXISTS (SELECT FROM information_schema.tables WHERE table_name = %s)'
            value = ('users', )
            self.standard_cur.execute(check_query, value)
            return self.standard_cur.fetchone()[0]

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Can\'t check content of database: \n{error}')

    def create_main_tbl(self):
        if not self.table_exist():
            try:
                table_query = '''
                    CREATE TABLE users (
                        user_id SERIAL PRIMARY KEY,
                        name VARCHAR NOT NULL,
                        password VARCHAR NOT NULL,
                        role VARCHAR NOT NULL DEFAULT %s,
                        msg_limit INTEGER NOT NULL DEFAULT %s,
                        msg_box VARCHAR[]
                    )
                '''
                values = ('user', 5)
                self.standard_cur.execute(table_query, values)
                self.conn.commit()

            except (Exception, Error) as error:
                self.conn.rollback()
                print(f'Unable to create main table in database: \n{error}')

    def add_administrator_to_db(self):
        if not 'admin' in self.get_users_names_from_db():
            try:
                self.add_new_user_in_db('admin', '1234')

                query = 'UPDATE users SET msg_limit = %s, role = %s WHERE name = %s'
                values = (100, 'administrator', 'admin')
                self.standard_cur.execute(query, values)
                self.conn.commit()

            except (Exception, Error) as error:
                self.conn.rollback()
                print(f'Unable to create administrator account in database: \n{error}')

    def add_new_user_in_db(self, name, pswrd):
        try:
            user_query = 'INSERT INTO users (name, password, role, msg_limit) VALUES (%s, %s, %s, %s)'
            values = (name, pswrd, 'user', 5)
            self.standard_cur.execute(user_query, values)
            self.conn.commit()

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Adding user to database failed: \n{error}')

    def get_users_names_from_db(self):
        try:
            query = 'SELECT name FROM users'
            self.standard_cur.execute(query)
            result = self.standard_cur.fetchall()
            corrected_result = [single_name[0] for single_name in result]
            return corrected_result

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Downloading users data failed: \n{error}')

    def get_users_data_from_db(self):
        try:
            query = 'SELECT * FROM users'
            self.standard_cur.execute(query)
            result = self.standard_cur.fetchall()
            return result

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Unable to extract users data from database: \n{error}')

    def get_single_user_data_from_db(self, name):
        try:
            query = 'SELECT * FROM users WHERE name = %s'
            value = (name,)
            self.standard_cur.execute(query, value)
            result = self.standard_cur.fetchone()
            return result

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Unable to extract single user data from database: \n{error}')

    def change_user_value_in_db(self, name, value_name, value):
        try:
            query = f'UPDATE users SET {value_name} = %s WHERE name = %s'
            values = (value, name)
            self.standard_cur.execute(query, values)
            self.conn.commit()

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Unable to change {value_name} in {name} data: \n{error}')

    def delete_data_from_db(self, name):
        try:
            query_user = 'DELETE FROM users WHERE name = %s'
            value = (name,)
            self.standard_cur.execute(query_user, value)
            self.conn.commit()

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Cannot delete {name} data: \n{error}')


class MessagesHandler:
    def __init__(self, database_handler):
        self.db_handler = database_handler
        self.standard_cur = self.db_handler.standard_cur
        self.dict_cur = self.db_handler.dict_cur
        self.conn = self.db_handler.conn

    def get_user_messages_from_db(self, name):
        try:
            query = 'SELECT msg_box FROM users WHERE name = %s'
            value = (name,)
            self.standard_cur.execute(query, value)
            result = self.standard_cur.fetchone()
            if result[0]:
                result_list = result[0]
                return result_list
            else:
                result = []
                return result

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Unable to extract user messages from database: \n{error}')

    def get_current_timestamp(self):
        try:
            query = 'SELECT to_char(now(), \'YYYY-MM-DD HH24:MI:SS\')'
            self.standard_cur.execute(query)
            result = self.standard_cur.fetchone()
            return result[0]

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Can\'t get current datetime from database: \n{error}')

    def insert_new_msg_to_user_msg_box(self, recipient, sender, message):
        try:
            query = 'UPDATE users SET msg_box = array_append(msg_box, %s) WHERE name = %s'
            values = (f'From: {sender}\n{self.get_current_timestamp()}\n{message}\n', recipient)
            self.standard_cur.execute(query, values)
            self.conn.commit()

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Unable to insert new message to user msg box in database: \n{error}')

    def delete_user_msg_from_db_by_id(self, name, msg_id):
        try:
            query = 'UPDATE users SET msg_box = array_remove(msg_box, msg_box[%s]) WHERE name = %s'
            values = (msg_id, name)
            self.standard_cur.execute(query, values)
            self.conn.commit()

        except (Exception, Error) as error:
            self.conn.rollback()
            print(f'Cannot delete message from {name} message box: \n{error}')


db_handler = DatabaseHandler()
db_handler.connect(config())

users_db = UserHandler(db_handler)
users_db.create_main_tbl()
users_db.add_administrator_to_db()

msg_db = MessagesHandler(db_handler)
