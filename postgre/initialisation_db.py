import configparser

config = configparser.ConfigParser()

config['DATABASE'] = {
    'host': '127.0.0.1',
    'port': '5000',
    'user': 'postgres',
    'password': 'junior',
    'database': 'users_db'
}

with open ('database.ini', 'w') as configfile:
    config.write(configfile)

