import sqlite3
from sqlite3 import Error


class DatabaseHandler:
    def __init__(self):
        self.connection = None
        self.cursor = None

    def connect(self):
        try:
            self.connection = sqlite3.connect('users_db_sqlite', check_same_thread=False)
            self.cursor = self.connection.cursor()

        except(Exception, Error) as error:
            print(f'Cannot connect with database. \n{error}')

    def close_connection(self):
        try:
            if self.connection is not None:
                self.cursor.close()
                self.connection.close()
                print('Database connection closed.')
                self.connection = None
                self.cursor = None

        except(Exception, Error) as error:
            print(f'Cannot close database connection \n{error}')


class UserHandler(DatabaseHandler):
    def __init__(self):
        super().__init__()
        self.connect()

    def create_main_tbl(self):
        try:
            table_query = '''
                CREATE TABLE IF NOT EXISTS users (
                    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name VARCHAR NOT NULL,
                    password VARCHAR NOT NULL,
                    role VARCHAR NOT NULL DEFAULT \'user\',
                    msg_limit INTEGER NOT NULL DEFAULT 5 ,
                    msg_box VARCHAR DEFAULT ''
                )
            '''
            self.cursor.execute(table_query)
            self.connection.commit()

        except(Exception, Error) as error:
            print(f'Cannot create a main table in database \n{error}')

    def get_users_names_from_db(self):
        try:
            query = 'SELECT name FROM users'
            self.cursor.execute(query)
            result = self.cursor.fetchall()
            corrected_result = [single_name[0] for single_name in result]
            return corrected_result

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Downloading users data failed: \n{error}')

    def add_new_user_in_db(self, name, pswrd):
        try:
            user_query = 'INSERT INTO users (name, password, role, msg_limit) VALUES (?, ?, ?, ?)'
            values = (name, pswrd, 'user', 5)
            self.cursor.execute(user_query, values)
            self.connection.commit()

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Adding user to database failed: \n{error}')

    def add_administrator_to_db(self):
        if 'admin' not in self.get_users_names_from_db():
            try:
                self.add_new_user_in_db('admin', '1234')
                query = 'UPDATE users SET msg_limit = ?, role = ? WHERE name = ?'
                values = (100, 'administrator', 'admin')
                self.cursor.execute(query, values)
                self.connection.commit()

            except (Exception, Error) as error:
                self.connection.rollback()
                print(f'Unable to create administrator account in database: \n{error}')

    def get_users_data_from_db(self):
        try:
            query = 'SELECT * FROM users'
            self.cursor.execute(query)
            result = self.cursor.fetchall()
            return result

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Unable to extract users data from database: \n{error}')

    def get_single_user_data_from_db(self, name):
        try:
            query = 'SELECT * FROM users WHERE name = ?'
            value = (name,)
            self.cursor.execute(query, value)
            result = self.cursor.fetchone()
            return result

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Unable to extract single user data from database: \n{error}')

    def change_user_value_in_db(self, name, value_name, value):
        try:
            query = f'UPDATE users SET {value_name} = ? WHERE name = ?'
            values = (value, name)
            self.cursor.execute(query, values)
            self.connection.commit()

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Unable to change {value_name} in {name} data: \n{error}')

    def delete_data_from_db(self, name):
        try:
            query_user = 'DELETE FROM users WHERE name = ?'
            value = (name,)
            self.cursor.execute(query_user, value)
            self.connection.commit()

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Cannot delete {name} data: \n{error}')


class MessagesHandler(DatabaseHandler):
    def __init__(self):
        super().__init__()
        self.connect()

    def get_user_messages_from_db(self, name):
        try:
            query = 'SELECT msg_box FROM users WHERE name = ?'
            value = (name,)
            self.cursor.execute(query, value)
            result = self.cursor.fetchone()
            if result[0]:
                result_list = [msg.split('*new_msg*') for msg in result][0]
                return result_list
            else:
                result = []
                return result

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Unable to extract user messages from database: \n{error}')

    def clear_empty_msg(self, name):
        messages = self.get_user_messages_from_db(name)
        cleared = [msg for msg in messages if msg.strip()]
        result = '*new_msg*'.join(cleared)

        query = 'UPDATE users SET msg_box = ? WHERE name = ?'
        values = (result, name)
        self.cursor.execute(query, values)
        self.connection.commit()

    def get_current_timestamp(self):
        try:
            query = 'SELECT strftime(\'%Y-%m-%d %H:%M:%S\', \'now\')'
            self.cursor.execute(query)
            result = self.cursor.fetchone()
            return result[0]

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Can\'t get current datetime from database: \n{error}')

    def insert_new_msg_to_user_msg_box(self, recipient, sender, message):
        try:
            query = 'UPDATE users SET msg_box = ? || \'*new_msg*\' || msg_box WHERE name = ?'
            values = (f'From: {sender}\n{self.get_current_timestamp()}\n{message}\n', recipient)
            self.cursor.execute(query, values)
            self.connection.commit()
            self.clear_empty_msg(recipient)

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Unable to insert new message to user msg box in database: \n{error}')

    def delete_user_msg_from_db_by_id(self, name, msg_id):
        try:
            messages_list = self.get_user_messages_from_db(name)
            after_deleted = [msg for pos, msg in enumerate(messages_list, start=1) if pos != int(msg_id)]
            result = '*new_msg*'.join(after_deleted)

            query = 'UPDATE users SET msg_box = ? WHERE name = ?'
            values = (result, name)
            self.cursor.execute(query, values)
            self.connection.commit()

        except (Exception, Error) as error:
            self.connection.rollback()
            print(f'Cannot delete message from {name} message box: \n{error}')


db_handler = DatabaseHandler()
db_handler.connect()

users_db = UserHandler()
users_db.create_main_tbl()
users_db.add_administrator_to_db()

msg_db = MessagesHandler()
