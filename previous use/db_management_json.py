import json
from pathlib import Path

class DataBase():
    def __init__(self):
        self.db_path = Path.cwd() / "users_files/users_data.json"

    def load_from_db_json_file(self):
        with open(self.db_path, 'r') as db_json_file:
            data = json.load(db_json_file)
            return data

    def save_to_db_json_file(self, content):
        with open(self.db_path, 'w') as db_json_file:
            json.dump(content, db_json_file, indent=4)

    def get_users_list_from_db(self):
        result = []
        for factor in self.load_from_db_json_file():
            result.append(factor['name'])
        return result


db = DataBase()
