from postgre.users_db import *
from config_test_db import test_config
import unittest


class TestUsersDb(unittest.TestCase):
    def setUp(self):
        self.test_db = DatabaseHandler()
        self.test_db.connect(test_config())

        self.users_db = UserHandler(self.test_db)
        self.users_db.create_main_tbl()
        self.users_db.add_administrator_to_db()
        self.users_db.add_new_user_in_db('test', '1111')

        self.msg_db = MessagesHandler(self.test_db)

    def test_connect(self):
        self.assertEqual(self.test_db.conn.closed, 0)

    def test_cursors(self):
        self.assertIsNotNone(self.test_db.standard_cur)
        self.assertIsNotNone(self.test_db.dict_cur)

    def test_close_connection(self):
        self.test_db.close_connection()
        self.assertEqual(self.test_db.conn.closed, 1)

    def test_cursors_after_closing_connection(self):
        self.test_db.close_connection()
        self.assertEqual(self.test_db.standard_cur.closed, 1)
        self.assertEqual(self.test_db.dict_cur.closed, 1)

    def test_create_table(self):
        self.users_db.create_main_tbl()
        self.assertTrue(self.users_db.table_exist())

    def test_add_administrator_to_db(self):
        self.users_db.add_administrator_to_db()
        self.assertIn('admin', self.users_db.get_users_names_from_db())

    def test_add_new_user_in_db(self):
        self.users_db.add_new_user_in_db('example', 'some_password')
        self.assertIn('example', self.users_db.get_users_names_from_db())

    def test_get_users_names_from_db(self):
        result = self.users_db.get_users_names_from_db()
        self.assertIn('test', result)

    def test_get_users_data_from_db(self):
        result = self.users_db.get_users_data_from_db()
        self.assertIsInstance(result, list)
        for factor in result:
            if factor['name'] == 'admin':
                self.assertEqual(factor['name'], 'admin')
            if factor['name'] == 'test':
                self.assertEqual(factor['name'], 'test')
            else:
                return False

    def test_single_user_data_from_db(self):
        result = self.users_db.get_single_user_data_from_db('test')
        self.assertIsInstance(result, tuple)
        self.assertIsInstance(result[0], int)
        self.assertEqual(result[1], 'test')
        self.assertIsInstance(result[2], str)
        self.assertIsInstance(result[3], str)
        self.assertIsInstance(result[4], int)

    def test_get_current_timestamp(self):
        result = self.msg_db.get_current_timestamp()
        self.assertIsInstance(result, str)

    def test_insert_new_msg_to_user_msg_box(self):
        self.msg_db.insert_new_msg_to_user_msg_box('test', 'some sender', 'some message')
        result = self.msg_db.get_user_messages_from_db('test')
        self.assertTrue(len(result) > 0)

    def test_get_user_messages_from_db(self):
        result = self.msg_db.get_user_messages_from_db('test')
        self.assertIsInstance(result, list)

    def test_delete_user_msg_from_db_by_id(self):
        self.msg_db.insert_new_msg_to_user_msg_box('test', 'someone', 'some message')
        before_deleting = len(self.msg_db.get_user_messages_from_db('test'))

        self.msg_db.delete_user_msg_from_db_by_id('test', 1)
        after_deleting = len(self.msg_db.get_user_messages_from_db('test'))

        self.assertNotEqual(before_deleting, after_deleting)

    def test_change_user_value_in_db(self):
        self.users_db.change_user_value_in_db('test', 'role', 'administrator')
        result = self.users_db.get_single_user_data_from_db('test')
        self.assertTrue(result[3] != 'user')

    def test_delete_data_from_db(self):
        self.users_db.delete_data_from_db('test')
        self.assertNotIn('test', self.users_db.get_users_names_from_db())

    def tearDown(self):
        try:
            for name in self.users_db.get_users_names_from_db():
                self.users_db.delete_data_from_db(name)

            self.test_db.close_connection()

        except Exception as e:
            print(f'Error in tearDown: {e}')


if __name__ == '__main__':
    unittest.main()
