import unittest
from users_management import User
from postgre.users_db import db_handler, users_db, msg_db
from postgre.config_db import config


class TestUsersManagement(unittest.TestCase):

    def setUp(self):
        db_handler.connect(config())
        users_db.create_main_tbl()

        users_db.add_new_user_in_db('test_admin', '1234')
        users_db.change_user_value_in_db('test_admin', 'role', 'administrator')
        self.test_admin = User('test_admin', '1234', 'administrator')
        self.test_admin.log_in('test_admin', '1234')

        users_db.add_new_user_in_db('test_user', '4321')
        self.test_user = User('test_user', '4321', 'user')

        self.temp_logged_out = User(None, None, None)
        self.temp_logged_out.logged_in = False

    def tearDown(self):
        users_db.delete_data_from_db('test_admin')
        users_db.delete_data_from_db('test_user')

    def test_grant_permission_logged_out(self):
        self.temp_logged_out.grant_permissions()
        self.assertEqual(len(self.temp_logged_out.commands), 3)
        self.assertEqual(self.temp_logged_out.msg_limit, 0)

    def test_grant_permission_user(self):
        self.test_user.log_in('test_user', '4321')
        self.assertEqual(len(self.test_user.commands), 5)
        self.assertEqual(self.test_user.msg_limit, 5)

    def test_grant_permission_admin(self):
        self.test_admin.grant_permissions()
        self.assertEqual(len(self.test_admin.commands), 12)
        self.assertEqual(self.test_admin.msg_limit, 100)

    def test_add_user_data(self):
        self.test_admin.add_user_data('added_user', '1111')
        self.assertIn('added_user', users_db.get_users_names_from_db())
        users_db.delete_data_from_db('added_user')

    def test_login_failure(self):
        result = self.temp_logged_out.log_in('wrong_name', 'some_pswrd')
        self.assertFalse(result)

    def test_login_successful(self):
        result = self.temp_logged_out.log_in('test_user', '4321')
        self.assertTrue(result)

    def test_sign_up_failure(self):
        result = self.temp_logged_out.sign_up('test_user', '4321')
        self.assertFalse(result)

    def test_sign_up_successful(self):
        result = self.temp_logged_out.sign_up('new_example_user', '0000')
        self.assertTrue(result)
        users_db.delete_data_from_db('new_example_user')

    def test_log_out(self):
        self.test_admin.log_out()
        self.assertEqual(self.test_admin.name, None)
        self.assertEqual(self.test_admin.password, None)
        self.assertEqual(self.test_admin.role, None)
        self.assertEqual(len(self.test_admin.msg_box), 0)
        self.assertEqual(self.test_admin.msg_limit, 0)
        self.assertFalse(self.test_admin.logged_in)
        self.assertEqual(len(self.test_admin.commands), 3)

    def test_check_picked_user_exist(self):
        result = self.test_admin.check_picked_user_exist('test_user')
        self.assertTrue(result)

    def test_check_picked_user_exist_not(self):
        result = self.test_admin.check_picked_user_exist('non-existent_user')
        self.assertFalse(result)

    def test_evaluate_message_data_successful(self):
        result = self.test_admin.evaluate_message_data('example message')
        self.assertTrue(result)

    def test_evaluate_message_data_failure(self):
        result = self.test_admin.evaluate_message_data('example message' * 20)
        self.assertFalse(result)

    def test_check_msg_box_overflow_free(self):
        result = self.test_admin.check_msg_box_overflow('test_user')
        self.assertFalse(result)

    def test_send_message_failure(self):
        result = self.test_admin.send_message('test_user', 'x' * 256)
        self.assertFalse(result)

    def test_check_messages_presence(self):
        msg_db.insert_new_msg_to_user_msg_box('test_user', 'admin', 'some message')
        result = self.test_user.check_messages()
        self.assertTrue(result)

    def test_check_messages_empty(self):
        result = self.test_user.check_messages()
        self.assertFalse(result)

    def test_show_messages(self):
        result = self.test_admin.show_messages()
        self.assertIsInstance(result, str)

    def test_check_int_input_failure(self):
        result = self.test_admin.check_int_input('1.23')
        self.assertFalse(result)

    def test_check_int_input_successful(self):
        result = self.test_admin.check_int_input('10')
        self.assertTrue(result)

    def test_delete_message_failure(self):
        result = self.test_admin.delete_message('200')
        self.assertFalse(result)

    def test_delete_message_successful(self):
        msg_db.insert_new_msg_to_user_msg_box('test_user', 'someone', 'example message')
        result = self.test_user.delete_message('1')
        self.assertTrue(result)

    def test_change_password(self):
        old_pswrd = users_db.get_single_user_data_from_db('test_user')[2]
        self.test_admin.change_password('test_user', '2222')
        new_pswrd = users_db.get_single_user_data_from_db('test_user')[2]
        self.assertNotEqual(old_pswrd, new_pswrd)

    def test_delete_user(self):
        self.assertIn('test_user', users_db.get_users_names_from_db())
        self.test_admin.delete_user('test_user')
        self.assertNotIn('test_user', users_db.get_users_names_from_db())

    def test_change_role_failure(self):
        result = self.test_admin.change_role('test_user', 'wrong_role')
        self.assertFalse(result)

    def test_change_role_successful(self):
        result = self.test_admin.change_role('test_user', 'administrator')
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()
