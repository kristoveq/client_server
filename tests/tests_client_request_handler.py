import unittest
from unittest.mock import patch
from client_request_handler import RequestHandler


class TestRequestHandler(unittest.TestCase):

    def setUp(self):
        self.handler = RequestHandler('client_object')

    def tearDown(self):
        del self.handler

    def test_initialization(self):
        self.assertEqual('client_object', self.handler.client)

    @patch('builtins.input', side_effect=['test_nickname', 'test_password'])
    def test_login_or_signup_rqst(self, mock_input):
        result = self.handler.login_or_signup_rqst('login')
        self.assertEqual(result, 'login,test_nickname,test_password')

    @patch('builtins.input', side_effect=['wrong_nickname', 'wrong_password'])
    def test_login_or_signup_rqst_failure(self, mock_input):
        result = self.handler.login_or_signup_rqst('login')
        self.assertNotEqual(result, 'login,test_nickname,test_password')

    @patch('builtins.input', side_effect=['', 'test_password'])
    def test_login_or_signup_rqst_empty_name(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.login_or_signup_rqst('login')

    @patch('builtins.input', side_effect=['test_nickname', ''])
    def test_login_or_signup_rqst_empty_password(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.login_or_signup_rqst('login')

    @patch('builtins.input', side_effect=['test_recipient', 'test_message'])
    def test_send_rqst(self, mock_input):
        result = self.handler.send_rqst('send')
        self.assertEqual(result, 'send,test_recipient,test_message')

    @patch('builtins.input', side_effect=['wrong_recipient', 'wrong_message'])
    def test_send_rqst_failure(self, mock_input):
        result = self.handler.send_rqst('send')
        self.assertNotEqual(result, 'send,test_recipient,test_message')

    @patch('builtins.input', side_effect=['', 'test_message'])
    def test_send_rqst_empty_recipient(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.send_rqst('send')

    @patch('builtins.input', side_effect=['test_recipient', ''])
    def test_send_rqst_empty_message(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.send_rqst('send')

    @patch('builtins.input', side_effect=['1'])
    def test_delete_rqst(self, mock_input):
        result = self.handler.delete_rqst('delete')
        self.assertEqual(result, 'delete,1')

    @patch('builtins.input', side_effect=['5'])
    def test_delete_rqst_failure(self, mock_input):
        result = self.handler.delete_rqst('delete')
        self.assertNotEqual(result, 'delete,1')

    @patch('builtins.input', side_effect=[''])
    def test_delete_rqst_empty_id(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.delete_rqst('delete')

    @patch('builtins.input', side_effect=['test_name', 'test_pswrd'])
    def test_add_rqst(self, mock_input):
        result = self.handler.add_rqst('add')
        self.assertEqual(result, 'add,test_name,test_pswrd')

    @patch('builtins.input', side_effect=['wrong_name', 'wrong_pswrd'])
    def test_add_rqst_failure(self, mock_input):
        result = self.handler.add_rqst('add')
        self.assertNotEqual(result, 'add,test_name,test_pswrd')

    @patch('builtins.input', side_effect=['', 'test_pswrd'])
    def test_add_rqst_empty_name(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.add_rqst('add')

    @patch('builtins.input', side_effect=['test_name', ''])
    def test_add_rqst_empty_password(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.add_rqst('add')

    @patch('builtins.input', side_effect=['test_name', 'test_password'])
    def test_change_password_rqst(self, mock_input):
        result = self.handler.change_password_rqst('change_password')
        self.assertEqual(result, 'change_password,test_name,test_password')

    @patch('builtins.input', side_effect=['wrong_name', 'wrong_password'])
    def test_change_password_rqst_failure(self, mock_input):
        result = self.handler.change_password_rqst('change_password')
        self.assertNotEqual(result, 'change_password,test_name,test_password')

    @patch('builtins.input', side_effect=['', 'test_password'])
    def test_change_password_rqst_empty_name(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.change_password_rqst('change_password')

    @patch('builtins.input', side_effect=['test_name', ''])
    def test_change_password_rqst_empty_password(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.change_password_rqst('change_password')

    @patch('builtins.input', side_effect=['test_name'])
    def test_del_user_rqst(self, mock_input):
        result = self.handler.del_user_rqst('del_user')
        self.assertEqual(result, 'del_user,test_name')

    @patch('builtins.input', side_effect=['wrong_name'])
    def test_del_user_rqst_failure(self, mock_input):
        result = self.handler.del_user_rqst('del_user')
        self.assertNotEqual(result, 'del_user,test_name')

    @patch('builtins.input', side_effect=[''])
    def test_del_user_rqst_empty_name(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.del_user_rqst('del_user')

    @patch('builtins.input', side_effect=['test_name', 'test_role'])
    def test_set_role_rqst(self, mock_input):
        result = self.handler.set_role_rqst('set_role')
        self.assertEqual(result, 'set_role,test_name,test_role')

    @patch('builtins.input', side_effect=['wrong_name', 'wrong_role'])
    def test_set_role_rqst_failure(self, mock_input):
        result = self.handler.set_role_rqst('set_role')
        self.assertNotEqual(result, 'set_role,test_name,test_role')

    @patch('builtins.input', side_effect=['', 'test_role'])
    def test_set_role_rqst_empty_name(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.set_role_rqst('set_role')

    @patch('builtins.input', side_effect=['test_name', ''])
    def test_set_role_rqst_empty_role(self, mock_input):
        with self.assertRaises(ValueError):
            self.handler.set_role_rqst('set_role')


if __name__ == '__main__':
    unittest.main()
