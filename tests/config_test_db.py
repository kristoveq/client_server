import os.path
from configparser import ConfigParser


def test_config(filename="test_database.ini", section='TEST_DATABASE'):
    current_dir = os.path.dirname(os.path.abspath(__file__))
    full_path = os.path.join(current_dir, filename)

    parser = ConfigParser()
    parser.read(full_path)

    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception(f'Section {section} not found in the {filename} file.')
    print(db)
    return db
