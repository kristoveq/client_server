import unittest
from unittest.mock import patch
from server_response_handler import ResponseHandler
import json


class TestResponseHandler(unittest.TestCase):

    def setUp(self):
        self.handler = ResponseHandler('server', 'user')

    def tearDown(self):
        del self.handler

    def test_initialization(self):
        self.assertEqual('server', self.handler.server)
        self.assertEqual('user', self.handler.user)

    def test_unrecognized_command_re(self):
        result = self.handler.unrecognized_command_re()
        expected_result = json.dumps('Unrecognized command.')
        self.assertEqual(result, expected_result)



if __name__ == '__main__':
    unittest.main()