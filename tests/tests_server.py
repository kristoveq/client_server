import socket
import unittest
from unittest.mock import patch, MagicMock
from server import Server
import json

class TestServer(unittest.TestCase):

    def setUp(self):
        patcher = patch('socket.socket')
        self.addCleanup(patcher.stop)
        self.mock_socket = patcher.start()

        self.mock_socket_instance = MagicMock()
        self.mock_socket.return_value = self.mock_socket_instance

        response_handler_patcher = patch('server.ResponseHandler')
        self.addCleanup(response_handler_patcher.stop)
        self.mock_response_handler = response_handler_patcher.start()
        self.mock_response_handler_instance = self.mock_response_handler.return_value

        self.mock_user = patch('server.user', autospec=True).start()
        self.addCleanup(patch.stopall)

        self.mock_socket_instance.accept.return_value = (MagicMock, ('127.0.0.1', 11111))

        self.server = Server('127.0.0.1', 61234)

    def tearDown(self):
        del self.server

    def test_initialization(self):
        self.mock_socket.assert_called_once_with(socket.AF_INET, socket.SOCK_STREAM)
        self.assertEqual(self.server.host, '127.0.0.1')
        self.assertEqual(self.server.port, 61234)
        self.assertEqual(self.server.BUFFER, 1024)
        self.assertIn('Version:', self.server.server_info)

    def test_connect(self):
        result = self.server.connect()

        self.mock_socket_instance.bind.assert_called_once_with(('127.0.0.1', 61234))
        self.mock_socket_instance.listen.assert_called_once_with(3)
        self.mock_socket_instance.accept.assert_called_once()

        self.assertTrue(result)

    def test_connect_socket_error(self):
        self.mock_socket_instance.bind.side_effect = socket.error('Socket error')
        result = self.server.connect()

        self.mock_socket_instance.bind.assert_called_once_with(('127.0.0.1', 61234))
        self.assertFalse(result)
        self.mock_socket_instance.close.assert_called_once()

    def test_connect_general_error(self):
        self.mock_socket_instance.bind.side_effect = Exception('General exception')
        result = self.server.connect()

        self.mock_socket_instance.bind.assert_called_once_with(('127.0.0.1', 61234))
        self.assertFalse(result)
        self.mock_socket_instance.close.assert_called_once()

    def test_take_request(self):
        self.mock_socket_instance.recv.return_value = b'test'
        self.server.client_socket = self.mock_socket_instance
        result = self.server.take_request()
        self.assertEqual(result, 'test')

    def test_take_request_socket_error(self):
        self.mock_socket_instance.recv.side_effect = socket.error('Socket error')
        self.server.client_socket = self.mock_socket_instance
        result = self.server.take_request()

        self.assertFalse(result)
        self.mock_socket_instance.recv.assert_called_once()
        self.mock_socket_instance.close.assert_called_once()

    def test_take_request_connection_error(self):
        self.mock_socket_instance.recv.side_effect = ConnectionResetError('Connection error')
        self.server.client_socket = self.mock_socket_instance
        result = self.server.take_request()

        self.assertFalse(result)
        self.mock_socket_instance.recv.assert_called_once()
        self.mock_socket_instance.close.assert_called_once()

    def test_take_request_general_error(self):
        self.mock_socket_instance.recv.side_effect = Exception('General error')
        self.server.client_socket = self.mock_socket_instance
        result = self.server.take_request()

        self.assertFalse(result)
        self.mock_socket_instance.recv.assert_called_once()
        self.mock_socket_instance.close.assert_called_once()

    def test_server_prepare_response_commands(self):
        commands_to_test = [
            ('uptime', 'uptime_re'),
            ('info', 'info_re'),
            ('help', 'help_re'),
            ('stop', 'stop_re'),
            ('login', 'login_re'),
            ('signup', 'sign_up_re'),
            ('send', 'send_re'),
            ('check', 'check_re'),
        ]
        for command, expected_return in commands_to_test:
            # creating subtests for each command to test.
            with self.subTest(command=command):
                method_name = expected_return
                mock_method = getattr(self.mock_response_handler_instance, method_name)
                mock_method.return_value = expected_return
                result = self.server.prepare_response(command)

                mock_method.assert_called_once_with(command)
                self.assertEqual(result, expected_return)

    def test_server_prepare_response_unrecognized_command(self):
        self.mock_response_handler_instance.unrecognized_command_re.return_value = 'unrecognized_command'
        result = self.server.prepare_response('unknown_command')
        self.mock_response_handler_instance.unrecognized_command_re.assert_called_once()
        self.assertEqual(result, 'unrecognized_command')

    def test_send_commands(self):
        self.mock_user.commands = {'command1': 'description1', 'command2': 'description2'}
        self.mock_send_response = patch.object(Server, 'send_response').start()
        self.server.send_commands()
        expected_json = json.dumps(['command1', 'command2'])
        self.mock_send_response.assert_called_once_with(expected_json)

    def test_send_commands_type_error(self):
        class UnserializableObject:
            pass

        self.mock_send_response = patch.object(Server, 'send_response').start()
        self.mock_user.commands = {UnserializableObject(): 'description'}
        result = self.server.send_commands()
        self.mock_send_response.assert_called_once_with(json.dumps({'Error': 'Failed to serialize commands.'}))
        self.assertFalse(result)

    def test_send_commands_connection_error(self):
        self.mock_send_response = patch.object(Server, 'send_response').start()
        self.mock_send_response.side_effect = ConnectionResetError('Connection reset by peer')
        self.mock_user.commands = {'command1': 'description1', 'command2': 'description2'}
        self.server.send_commands()

        self.mock_socket_instance.close.assert_called_once()

    def test_send_response(self):
        self.mock_client_socket = MagicMock()
        self.server.client_socket = self.mock_client_socket
        response = 'Test message'
        self.server.send_response(response)

        self.mock_client_socket.send.assert_called_once_with(response.encode('utf-8'))

    def test_send_response_connection_reset(self):
        self.mock_client_socket = MagicMock()
        self.server.client_socket = self.mock_client_socket
        self.mock_client_socket.send.side_effect = ConnectionResetError

        self.mock_server_socket = MagicMock()
        self.server.server_socket = self.mock_server_socket
        response = 'Test message'
        self.server.send_response(response)

        self.mock_server_socket.close.assert_called_once()


if __name__ == '__main__':
    unittest.main()
