import json
import socket
import unittest
from unittest.mock import patch, MagicMock
from client import Client


class TestClient(unittest.TestCase):

    def setUp(self):
        patcher = patch('socket.socket')
        self.addCleanup(patcher.stop)
        self.mock_socket = patcher.start()

        self.mock_socket_instance = MagicMock()
        self.mock_socket.return_value = self.mock_socket_instance

        request_handler_patcher = patch('client.RequestHandler')
        self.addCleanup(request_handler_patcher.stop)
        self.mock_request_handler = request_handler_patcher.start()
        self.mock_request_handler_instance = self.mock_request_handler.return_value

        self.client = Client('127.0.0.1', 61234)

    def tearDown(self):
        del self.client

    def test_client_initialization(self):
        self.mock_socket.assert_called_once_with(socket.AF_INET, socket.SOCK_STREAM)

        self.assertEqual(self.client.host, '127.0.0.1')
        self.assertEqual(self.client.port, 61234)
        self.assertEqual(self.client.BUFFER, 1024)
        self.assertEqual(self.client.client_socket, self.mock_socket_instance)

    def test_client_connect(self):
        self.client.connect()
        self.mock_socket_instance.connect.assert_called_once_with(('127.0.0.1', 61234))

    def test_client_connect_connection_error(self):
        self.mock_socket_instance.connect.side_effect = ConnectionRefusedError
        result = self.client.connect()

        self.mock_socket_instance.connect.assert_called_once_with(('127.0.0.1', 61234))
        self.assertFalse(result)

    def test_client_connect_timeout_error(self):
        self.mock_socket_instance.connect.side_effect = TimeoutError
        result = self.client.connect()

        self.mock_socket_instance.connect.assert_called_once_with(('127.0.0.1', 61234))
        self.assertFalse(result)

    def test_client_connect_general_error(self):
        self.mock_socket_instance.connect.side_effect = Exception
        result = self.client.connect()

        self.mock_socket_instance.connect.assert_called_once_with(('127.0.0.1', 61234))
        self.assertFalse(result)

    def test_client_send_request(self):
        self.client.send_request('test')
        self.mock_socket_instance.send.assert_called_once_with(b'test')

    def test_client_send_request_os_error(self):
        self.mock_socket_instance.send.side_effect = OSError
        result = self.client.send_request('test')

        self.mock_socket_instance.send.assert_called_once_with(b'test')
        self.assertFalse(result)

    def test_client_send_request_general_error(self):
        self.mock_socket_instance.send.side_effect = Exception
        result = self.client.send_request('test')

        self.mock_socket_instance.send.assert_called_once_with(b'test')
        self.assertFalse(result)

    @patch('json.loads', return_value={'key': 'value'})
    def test_client_take_response(self, mock_json_loads):
        self.client.client_socket.recv.return_value = '{"key": "value"}'.encode('UTF-8')
        self.client.take_response()
        mock_json_loads.assert_called_once_with('{"key": "value"}')

    def test_client_take_response_socket_error(self):
        self.client.client_socket.recv.side_effect = socket.error('Simulated socket error.')
        result = self.client.take_response()
        self.assertFalse(result)

    def test_client_take_response_decode_error(self):
        self.client.client_socket.recv.side_effect = json.JSONDecodeError('Simulated json decode error.',
                                                                          '{"key": "value"}',
                                                                          0)
        result = self.client.take_response()
        self.assertFalse(result)

    def test_client_take_response_general_error(self):
        self.client.client_socket.recv.side_effect = Exception
        result = self.client.take_response()
        self.assertFalse(result)

    @patch('json.loads', return_value={'key': 'value'})
    def test_client_update_commands(self, mock_json_loads):
        self.client.client_socket.recv.return_value = '{"key": "value"}'.encode('UTF-8')

        result = self.client.update_commands()
        mock_json_loads.assert_called_once_with('{"key": "value"}')
        self.assertEqual(result, {'key': 'value'})

    def test_client_update_commands_decode_error(self):
        self.client.client_socket.recv.side_effect = json.JSONDecodeError('Simulated json decode error.',
                                                                          '{"key": "value"}',
                                                                          0)
        result = self.client.update_commands()
        self.assertFalse(result)

    def test_client_update_commands_general_error(self):
        self.client.client_socket.recv.side_effect = Exception
        result = self.client.update_commands()
        self.assertFalse(result)

    def test_client_prepare_request_login(self):
        self.mock_request_handler_instance.login_or_signup_rqst.return_value = 'login_response'
        result = self.client.prepare_request('login')
        self.mock_request_handler_instance.login_or_signup_rqst.assert_called_once_with('login')
        self.assertEqual(result, 'login_response')

    def test_client_prepare_request_signup(self):
        self.mock_request_handler_instance.login_or_signup_rqst.return_value = 'signup_response'
        result = self.client.prepare_request('signup')
        self.mock_request_handler_instance.login_or_signup_rqst.assert_called_once_with('signup')
        self.assertEqual(result, 'signup_response')

    def test_client_prepare_request_send(self):
        self.mock_request_handler_instance.send_rqst.return_value = 'send_response'
        result = self.client.prepare_request('send')
        self.mock_request_handler_instance.send_rqst.assert_called_once_with('send')
        self.assertEqual(result, 'send_response')

    def test_client_prepare_request_delete(self):
        self.mock_request_handler_instance.delete_rqst.return_value = 'delete_response'
        result = self.client.prepare_request('delete')
        self.mock_request_handler_instance.delete_rqst.assert_called_once_with('delete')
        self.assertEqual(result, 'delete_response')

    def test_client_prepare_request_add(self):
        self.mock_request_handler_instance.add_rqst.return_value = 'add_response'
        result = self.client.prepare_request('add')
        self.mock_request_handler_instance.add_rqst.assert_called_once_with('add')
        self.assertEqual(result, 'add_response')

    def test_client_prepare_request_change_password(self):
        self.mock_request_handler_instance.change_password_rqst.return_value = 'change_password_response'
        result = self.client.prepare_request('change_password')
        self.mock_request_handler_instance.change_password_rqst.assert_called_once_with('change_password')
        self.assertEqual(result, 'change_password_response')

    def test_client_prepare_request_change_del_user(self):
        self.mock_request_handler_instance.del_user_rqst.return_value = 'del_user_response'
        result = self.client.prepare_request('del_user')
        self.mock_request_handler_instance.del_user_rqst.assert_called_once_with('del_user')
        self.assertEqual(result, 'del_user_response')

    def test_client_prepare_request_change_set_role(self):
        self.mock_request_handler_instance.set_role_rqst.return_value = 'set_role_response'
        result = self.client.prepare_request('set_role')
        self.mock_request_handler_instance.set_role_rqst.assert_called_once_with('set_role')
        self.assertEqual(result, 'set_role_response')

    def test_client_prepare_request_key_error(self):
        result = self.client.prepare_request('Non existing command')
        self.assertFalse(result)

    def test_client_prepare_request_general_error(self):
        result = self.client.prepare_request(1111)
        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()
