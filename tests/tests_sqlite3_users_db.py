from sqlite3_users_db import db_handler, users_db, msg_db
import unittest


class TestSqliteUsersDb(unittest.TestCase):
    def setUp(self):
        self.db_handler = db_handler
        self.db_handler.connect()

        self.user_handler = users_db
        self.user_handler.connect()
        self.user_handler.create_main_tbl()
        self.user_handler.add_administrator_to_db()
        self.user_handler.add_new_user_in_db('test', '1111')

        self.msg_handler = msg_db
        self.msg_handler.connect()

    def test_connect(self):
        self.assertIsNotNone(db_handler.connection)
        self.assertIsNotNone(db_handler.cursor)
        self.assertIsNotNone(self.user_handler.connection)
        self.assertIsNotNone(self.msg_handler.connection)

    def test_closing_connection(self):
        self.db_handler.close_connection()
        self.assertIsNone(self.db_handler.connection)
        self.assertIsNone(self.db_handler.cursor)

        self.msg_handler.close_connection()
        self.assertIsNone(self.msg_handler.connection)
        self.assertIsNone(self.msg_handler.cursor)

        self.user_handler.close_connection()
        self.assertIsNone(self.user_handler.connection)
        self.assertIsNone(self.user_handler.cursor)

        self.user_handler.connect()  # opening connection to execute 'tearDown' method.

    def test_get_users_name_from_db(self):
        result = len(self.user_handler.get_users_names_from_db())
        self.assertEqual(result, 2)

    def test_failure_get_users_name_from_db(self):
        with self.assertRaises(Exception):
            self.user_handler.cursor.execute('SELECT name from invalid_name')
            self.user_handler.get_users_names_from_db()

    def test_add_new_user_in_db(self):
        self.user_handler.add_new_user_in_db('new_user', 'some_pswrd')
        result = self.user_handler.get_users_names_from_db()
        self.assertIn('new_user', result)

        self.user_handler.delete_data_from_db('new_user')

    def test_add_new_user_in_db_failure(self):
        with self.assertRaises(Exception):
            values = ('A', 'B', 'C', {'wrong': 'data_type'})
            self.user_handler.cursor.execute(
                'INSERT INTO users (name, password, role, msg_limit) VALUES (?, ?, ?, ?)', values)

    def test_add_administrator_to_db(self):
        result = self.user_handler.get_users_names_from_db()
        self.assertIn('admin', result)

    def test_add_administrator_to_db_failure(self):
        with self.assertRaises(Exception):
            values = (300, 'some_role', 'admin')
            self.user_handler.cursor.execute('UPDATE users SET msg_limit = ?, role = ? WHERE name = wrong_name', values)

    def test_get_users_data_from_db(self):
        result = self.user_handler.get_users_data_from_db()
        self.assertIsInstance(result, list)

    def test_get_users_data_from_db_failure(self):
        with self.assertRaises(Exception):
            self.user_handler.cursor.execute('SELECT * from wrong_table_name')

    def test_get_single_user_data_from_db(self):
        result = self.user_handler.get_single_user_data_from_db('test')
        self.assertIsInstance(result, tuple)

    def test_get_single_user_data_from_db_failure(self):
        with self.assertRaises(Exception):
            value = 'wrong name'
            self.user_handler.cursor.execute('SELECT * FROM users WHERE name = ?', value)

    def test_change_user_value_in_db(self):
        value = 'administrator'
        self.user_handler.change_user_value_in_db('test', 'role', value)
        result = self.user_handler.get_single_user_data_from_db('test')[3]
        self.assertEqual(result, value)

    def test_change_user_value_in_db_failure(self):
        with self.assertRaises(Exception):
            value_name = 'wrong_value'
            values = ('administrator', 'test')
            self.user_handler.cursor.execute(f'UPDATE users SET {value_name} = ? WHERE name = ?', values)

    def test_delete_data_from_db(self):
        self.user_handler.delete_data_from_db('test')
        result = len(self.user_handler.get_users_names_from_db())
        self.assertEqual(1, result)

    def test_delete_data_from_db_failure(self):
        with self.assertRaises(Exception):
            value = 'wrong_name'
            self.user_handler.cursor.execute('DELETE FROM users WHERE name = ?', value)

    def test_get_user_messages_from_db(self):
        result = self.msg_handler.get_user_messages_from_db('admin')
        self.assertEqual(0, len(result))
        self.assertIsInstance(result, list)

    def test_get_user_messages_from_db_failure(self):
        with self.assertRaises(Exception):
            value = 'wrong_name'
            self.msg_handler.cursor.execute('SELECT msg_box FROM users WHERE name = ?', value)

    def test_get_current_timestamp(self):
        result = self.msg_handler.get_current_timestamp()
        self.assertIsInstance(result, str)

    def test_get_current_timestamp_failure(self):
        with self.assertRaises(Exception):
            value = 'wrong_format'
            self.msg_handler.cursor.execute('SELECT ? (\'%Y-%m-%d %H:%M:%S\', \'now\')', value)

    def test_insert_new_msg_to_user_msg_box(self):
        before = len(self.msg_handler.get_user_messages_from_db('test'))
        self.msg_handler.insert_new_msg_to_user_msg_box('test', 'some_user', 'example message')

        after = len(self.msg_handler.get_user_messages_from_db('test'))
        self.assertNotEqual(before, after)

    def test_insert_new_msg_to_user_msg_box_failure(self):
        with self.assertRaises(Exception):
            name = 'wrong_name'
            self.msg_handler.cursor.execute('UPDATE users SET msg_box = \'sd\' || \'*new_msg*\' ||'
                                            ' msg_box WHERE name = ?', name)

    def test_delete_user_msg_from_db_by_id(self):
        self.msg_handler.insert_new_msg_to_user_msg_box('test', 'some user', 'example_msg')
        before = len(self.msg_handler.get_user_messages_from_db('test'))

        self.msg_handler.delete_user_msg_from_db_by_id('test', '1')
        after = len(self.msg_handler.get_user_messages_from_db('test'))

        self.assertNotEqual(before, after)

    def test_delete_user_msg_from_db_by_id_failure(self):
        with self.assertRaises(Exception):
            name = 'wrong_name'
            self.msg_handler.cursor.execute('UPDATE users SET msg_box = \'message\' WHERE name = ?', name)

    def tearDown(self):
        self.user_handler.delete_data_from_db('test')
        self.db_handler.close_connection()
        self.user_handler.close_connection()
        self.msg_handler.close_connection()


if __name__ == '__main__':
    unittest.main()