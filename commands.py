basic_commands =  {
        'help' : 'show list of possible actions with short description',
        'signup' : 'create a new account',
        'login' : 'log into your personal account',
        }

user_commands = {
        'help' : 'show list of possible actions with short description',
        'send' : 'send message to specific user',
        'check' : 'check message box',
        'delete' : 'delete specific message',
        'out' : 'log off'
}

admin_commands = {
        'uptime': 'show server lifetime',
        'info' : 'show server version and date of creation',
        'help' : 'show list of possible actions with short description',
        'stop' : 'stop connection',
        'add' : 'adding user',
        'change_password' : 'change password of specific account',
        'del_user' : 'delete user',
        'set_role' : 'set user/admin role'
}
