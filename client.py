import socket
import json
import sys
from client_request_handler import RequestHandler


class Client:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.BUFFER = 1024
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def connect(self):
        try:
            self.client_socket.connect((self.host, self.port))
        except ConnectionRefusedError as e:
            print(f'Error during trying to connect with server: {e}')
            return False
        except TimeoutError as e:
            print(f'Error: TIMEOUT - {e}')
            return False
        except Exception as e:
            print(f'Connection attempt error: {e}')

    def send_request(self, request):
        try:
            self.client_socket.send(request.encode('utf-8'))
        except OSError as e:
            print(f'System error: {e}')
            return False
        except Exception as e:
            print(f'Error occurred: - {e}')
            return False

    def take_response(self):
        try:
            response = json.loads(self.client_socket.recv(self.BUFFER).decode('utf-8'))
            print(response, end='\n\n')
        except socket.error as e:
            print(f'Socket error occurred: {e}')
            return False
        except json.JSONDecodeError as e:
            print(f'Error during decoding from JSON: {e}')
            return False
        except Exception as e:
            print(f'Error occurred: {e}')
            return False

    def update_commands(self):
        try:
            updated_commands = json.loads(self.client_socket.recv(self.BUFFER).decode('utf-8'))
            return updated_commands
        except json.JSONDecodeError as e:
            print(f'Error during decoding from JSON: {e}')
            return False
        except Exception as e:
            print(f'Error occurred: {e}')
            return False

    def prepare_request(self, client_request):
        request_handler = RequestHandler(self)
        request_methods = {
            'login': request_handler.login_or_signup_rqst,
            'signup': request_handler.login_or_signup_rqst,
            'send': request_handler.send_rqst,
            'delete': request_handler.delete_rqst,
            'add': request_handler.add_rqst,
            'change_password': request_handler.change_password_rqst,
            'del_user': request_handler.del_user_rqst,
            'set_role': request_handler.set_role_rqst
        }
        try:
            request_method = request_methods[client_request]
            return request_method(client_request)
        except KeyError as e:
            print(f'Key error occurred: {e}')
            return False
        except Exception as e:
            print(f'Error during prepare request: {e}')
            return False

HOST = '127.0.0.1'
PORT = 61234

if __name__ == '__main__':

    client = Client(HOST, PORT)
    client.connect()

    print('Type "help" for support:')
    while True:

        client_request = input()

        if client_request == "stop":
            print('App termination')
            client.send_request(client_request)
            client.client_socket.close()
            sys.exit()

        elif not client_request in client.update_commands():
            client.send_request('unrecognized_command')
        else:
            client_request = client.prepare_request(client_request)
            client.send_request(client_request)

        response = client.take_response()
