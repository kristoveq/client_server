class RequestHandler:

    def __init__(self, client):
        self.client = client

    def login_or_signup_rqst(self, request):
        name = str(input('Enter your nickname: '))
        pswrd = str(input('Enter password: '))
        if not name or not pswrd:
            raise ValueError('Nickname or password cannot be empty.')
        result = f'{request},{name},{pswrd}'
        return result

    def send_rqst(self, request):
        recipient = input('Who you want to send the message to?\n')
        message = input('Write a message:\n')
        if not recipient or not message:
            raise ValueError('Recipient or message cannot be empty.')
        result = f'{request},{recipient},{message}'
        return result

    def delete_rqst(self, request):
        msg_to_del = input('Type ID of the message you want to delete:\n')
        if not msg_to_del:
            raise ValueError('ID of the message cannot be empty.')
        result = f'{request},{msg_to_del}'
        return result

    def add_rqst(self, request):
        name = input('Enter nickname for new user:\n')
        pswrd = input('Enter password for new user:\n')
        if not name or not pswrd:
            raise ValueError('Nickname or password cannot be empty.')
        result = f'{request},{name},{pswrd}'
        return result

    def change_password_rqst(self, request):
        name = input('Enter nickname of user whose password you want to change:\n')
        new_pswrd = input(f'Enter new password for user "{name}":\n')
        if not name or not new_pswrd:
            raise ValueError('Name or new password cannot be empty.')
        result = f'{request},{name},{new_pswrd}'
        return result

    def del_user_rqst(self, request):
        name = input('Enter nickname of user you want to delete from database:\n')
        if not name:
            raise ValueError('Name cannot be empty.')
        result = f'{request},{name}'
        return result

    def set_role_rqst(self, request):
        name = input('Enter nickname of user whose role you want to change:\n')
        new_role = input(f'Enter new role for user "{name}":\n')
        if not name or not new_role:
            raise ValueError('Name or new role cannot be empty.')
        result = f'{request},{name},{new_role}'
        return result
